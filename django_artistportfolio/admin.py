from django.contrib import admin
from models import Series, Media, Category, Credit, CreditName, Cinema
from django.contrib.admin import TabularInline

class MediaInline(TabularInline):
    model = Media

class CreditInline(TabularInline):
    model = Credit

class BaseWYSIWYGAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    exclude = ('vimeo_meta',)

    class Media:
        css = {
                "all": (
                    "compiled-css/admin.css",
                    "css/bootstrap-wysihtml5.css",
                    )
            }
        js = (
                'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
                'js-lib/jquery.livequery.js',
                'js-lib/bootstrap-modal.js',
                'js-lib/wysihtml5-0.3.0.js',
                'js-lib/bootstrap-wysihtml5.js',
                'js/admin/textareas.js',
            )

class SeriesAdmin(BaseWYSIWYGAdmin):
    inlines = (
        MediaInline,
        CreditInline,
        )

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'position',)
    list_editable = ('position',)

    class Media:
        js = (
              'js-lib/jquery.min.js',
              'js-lib/jquery-ui.min.js',
              'js/admin/admin-list-reorder.js',
              )

admin.site.register(Category, CategoryAdmin)
admin.site.register(Series, SeriesAdmin)
admin.site.register(Cinema, BaseWYSIWYGAdmin)
admin.site.register(CreditName)