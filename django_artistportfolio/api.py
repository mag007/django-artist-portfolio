from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from tastypie import fields

from tastypie.constants import ALL, ALL_WITH_RELATIONS
from models import Series, Category, Media, Cinema
from feincms.module.medialibrary.models import MediaFile

class MediaFileResource(ModelResource):
    class Meta:
        authorization = Authorization()
        detail_allowed_methods = ['get']
        always_return_data = True

        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']

        queryset = MediaFile.objects.all()
        resource_name = 'media_file_resource'


class CinemaResource(ModelResource):
    video = fields.CharField()
    video_thumbnail = fields.CharField()

    def dehydrate_video(self, bundle):
        if bundle.obj.vimeo_meta:
            return bundle.obj.vimeo_meta.iframe

    def dehydrate_video_thumbnail(self, bundle):
        if bundle.obj.vimeo_meta:
            return bundle.obj.vimeo_meta.data.get('thumbnail_large', None)

    class Meta:
        authorization = Authorization()
        detail_allowed_methods = ['get']
        always_return_data = True

        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']

        queryset = Cinema.objects.all()
        resource_name = 'cinema_resource'

        filtering = {
            'slug':ALL
        }

class CategoryResource(ModelResource):
    class Meta:
        authorization = Authorization()
        detail_allowed_methods = ['get']
        always_return_data = True

        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']

        queryset = Category.objects.all()
        resource_name = 'category_resource'


class SeriesResource(ModelResource):
    category_name = fields.CharField()
    category_slug = fields.CharField()
    media_set = fields.ToManyField('deanbradshaw.apps.portfolio.api.MediaResource', 'media_set')
    behind_the_scenes = fields.CharField()
    behind_the_scenes_thumbnail = fields.CharField()
    credits = fields.CharField()

    def dehydrate_category_name(self, bundle):
        return bundle.obj.category.name

    def dehydrate_category_slug(self, bundle):
        return bundle.obj.category.slug

    def dehydrate_behind_the_scenes(self, bundle):
        if bundle.obj.vimeo_meta:
            return bundle.obj.vimeo_meta.iframe

    def dehydrate_behind_the_scenes_thumbnail(self, bundle):
        if bundle.obj.vimeo_meta:
            return bundle.obj.vimeo_meta.data.get('thumbnail_large', None)

    def dehydrate_credits(self, bundle):
        s = '<dl class="series-credits">'
        for credit in bundle.obj.credit_set.all():
            s+= '<dt>%s:</dt><dd>%s</dd>' % (credit.name.name, credit.value)

        s+= '</dl>'
        return s

    class Meta:
        authorization = Authorization()
        detail_allowed_methods = ['get']
        always_return_data = True

        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']

        queryset = Series.objects.all()
        resource_name = 'series_resource'

        filtering = {
            'media_set':ALL_WITH_RELATIONS,
            'slug':ALL
        }


class MediaResource(ModelResource):
    series = fields.ForeignKey(SeriesResource, 'series', null=True, blank=True)
    thumbnail = fields.CharField()
    normal = fields.CharField()
    zoomed = fields.CharField()
    category_name = fields.CharField()
    category_slug = fields.CharField()

    def dehydrate_category_name(self, bundle):
        return bundle.obj.series.category.name

    def dehydrate_category_slug(self, bundle):
        return bundle.obj.series.category.slug

    def dehydrate_thumbnail(self, bundle):
        try:
            return bundle.obj.thumbnail.file.url
        except:
            return ''

    def dehydrate_normal(self, bundle):
        try:
            return bundle.obj.normal.file.url
        except:
            return ''

    def dehydrate_zoomed(self, bundle):
        try:
            return bundle.obj.zoomed.file.url
        except:
            return ''

    class Meta:
        authorization = Authorization()
        detail_allowed_methods = ['get']
        always_return_data = True

        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']

        queryset = Media.objects.all()
        resource_name = 'media_resource'

        filtering = {
            'series':ALL_WITH_RELATIONS,
            'id': ALL,
        }