from models import Category

def portfolio_categories_context_processor(request):
    return { 'portfolio_categories' : Category.objects.all() }