from django.db.models.signals import pre_save
from django.dispatch import receiver
from models import VimeoMeta, Media, Series, Cinema
from resizer import Resizer
import urllib2, json

@receiver(pre_save, sender=Media)
def generate_sizes_for_media(sender, instance, **kwargs):
    if instance.zoomed and not instance.normal:
        resizer = Resizer(instance.zoomed.file, '2000x2000')
        mf = resizer.FeinCMS_MediaFile
        if mf:
            instance.normal = mf

    if instance.normal and not instance.thumbnail:
        resizer = Resizer(instance.normal.file, '240x10000')
        mf = resizer.FeinCMS_MediaFile
        if mf:
            instance.thumbnail = mf

@receiver(pre_save, sender=Series)
@receiver(pre_save, sender=Cinema)
def save_vimeo_meta(sender, instance, **kwargs):
    vimeo_id = None

    if hasattr(instance, 'behind_the_scenes') and instance.behind_the_scenes:
        vimeo_id = instance.behind_the_scenes.rstrip('/').rsplit('/',1).pop()
    elif hasattr(instance, 'video') and instance.video:
        vimeo_id = instance.video.rstrip('/').rsplit('/',1).pop()

    if vimeo_id:

        if instance.vimeo_meta:
            # We already have Vimeo Meta data saved, let's see if we've changed the url...
            if vimeo_id and int(instance.vimeo_meta.vimeo_id) == int(vimeo_id):
                # Same thing, let's abort...
                return
            else:
                # Hmmm, deleted the existing Vimeo url? No video?
                instance.vimeo_meta = None

        if vimeo_id:
            try:
                req = urllib2.Request("http://vimeo.com/api/v2/video/%s.json" % (vimeo_id), None, {'user-agent':'syncstream/vimeo'})
                opener = urllib2.build_opener()
                f = opener.open(req)
                json_string = f.read()
                li = json.loads(json_string)
                if len(li):
                    d = li.pop(0)
                    vimeo_meta = VimeoMeta.objects.create(vimeo_id=int(vimeo_id), data=d)
                    instance.vimeo_meta = vimeo_meta
            except:
                pass

    else:
        # Set the meta to None
        instance.vimeo_meta = None

