# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table('django_artistportfolio_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('position', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('django_artistportfolio', ['Category'])

        # Adding model 'VimeoMeta'
        db.create_table('django_artistportfolio_vimeometa', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vimeo_id', self.gf('django.db.models.fields.IntegerField')()),
            ('data', self.gf('jsonfield.fields.JSONField')(default={})),
        ))
        db.send_create_signal('django_artistportfolio', ['VimeoMeta'])

        # Adding model 'Series'
        db.create_table('django_artistportfolio_series', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_artistportfolio.Category'])),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('behind_the_scenes', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('vimeo_meta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_artistportfolio.VimeoMeta'], null=True, blank=True)),
        ))
        db.send_create_signal('django_artistportfolio', ['Series'])

        # Adding model 'CreditName'
        db.create_table('django_artistportfolio_creditname', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('django_artistportfolio', ['CreditName'])

        # Adding model 'Credit'
        db.create_table('django_artistportfolio_credit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('series', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_artistportfolio.Series'])),
            ('name', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_artistportfolio.CreditName'])),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('django_artistportfolio', ['Credit'])

        # Adding model 'Media'
        db.create_table('django_artistportfolio_media', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('series', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_artistportfolio.Series'])),
            ('thumbnail', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='thumbnail_set', null=True, to=orm['medialibrary.MediaFile'])),
            ('normal', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='normal_set', null=True, to=orm['medialibrary.MediaFile'])),
            ('zoomed', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='zoomed_set', null=True, to=orm['medialibrary.MediaFile'])),
        ))
        db.send_create_signal('django_artistportfolio', ['Media'])

        # Adding model 'Cinema'
        db.create_table('django_artistportfolio_cinema', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('video', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('vimeo_meta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_artistportfolio.VimeoMeta'], null=True, blank=True)),
        ))
        db.send_create_signal('django_artistportfolio', ['Cinema'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table('django_artistportfolio_category')

        # Deleting model 'VimeoMeta'
        db.delete_table('django_artistportfolio_vimeometa')

        # Deleting model 'Series'
        db.delete_table('django_artistportfolio_series')

        # Deleting model 'CreditName'
        db.delete_table('django_artistportfolio_creditname')

        # Deleting model 'Credit'
        db.delete_table('django_artistportfolio_credit')

        # Deleting model 'Media'
        db.delete_table('django_artistportfolio_media')

        # Deleting model 'Cinema'
        db.delete_table('django_artistportfolio_cinema')


    models = {
        'django_artistportfolio.category': {
            'Meta': {'ordering': "['position']", 'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'django_artistportfolio.cinema': {
            'Meta': {'object_name': 'Cinema'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'video': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'vimeo_meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_artistportfolio.VimeoMeta']", 'null': 'True', 'blank': 'True'})
        },
        'django_artistportfolio.credit': {
            'Meta': {'object_name': 'Credit'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_artistportfolio.CreditName']"}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_artistportfolio.Series']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'django_artistportfolio.creditname': {
            'Meta': {'object_name': 'CreditName'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'django_artistportfolio.media': {
            'Meta': {'object_name': 'Media'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'normal': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'normal_set'", 'null': 'True', 'to': "orm['medialibrary.MediaFile']"}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_artistportfolio.Series']"}),
            'thumbnail': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'thumbnail_set'", 'null': 'True', 'to': "orm['medialibrary.MediaFile']"}),
            'zoomed': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'zoomed_set'", 'null': 'True', 'to': "orm['medialibrary.MediaFile']"})
        },
        'django_artistportfolio.series': {
            'Meta': {'object_name': 'Series'},
            'behind_the_scenes': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_artistportfolio.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'vimeo_meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_artistportfolio.VimeoMeta']", 'null': 'True', 'blank': 'True'})
        },
        'django_artistportfolio.vimeometa': {
            'Meta': {'object_name': 'VimeoMeta'},
            'data': ('jsonfield.fields.JSONField', [], {'default': '{}'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vimeo_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'medialibrary.category': {
            'Meta': {'ordering': "['parent__title', 'title']", 'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['medialibrary.Category']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '150'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'medialibrary.mediafile': {
            'Meta': {'ordering': "['-created']", 'object_name': 'MediaFile'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['medialibrary.Category']", 'null': 'True', 'blank': 'True'}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255'}),
            'file_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '12'})
        }
    }

    complete_apps = ['django_artistportfolio']