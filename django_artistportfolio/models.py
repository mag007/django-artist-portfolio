from django.db import models
from feincms.content.application import models as app_models
from feincms.module.medialibrary.models import MediaFile
from jsonfield import JSONField

class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    position = models.IntegerField()

    def save(self, *args, **kwargs):
        model = self.__class__

        if self.position is None:
            # Append
            try:
                last = model.objects.order_by('-position')[0]
                self.position = last.position + 1
            except IndexError:
                # First row
                self.position = 0

        return super(Category, self).save(*args, **kwargs)

    class Meta:
        ordering = ['position']

    def __unicode__(self):
        return '%s' % (self.name)

class VimeoMeta(models.Model):
    vimeo_id = models.IntegerField()
    data = JSONField()

    @property
    def iframe(self):
        template = '<iframe src="http://player.vimeo.com/video/%s?title=0&byline=0&portrait=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'
        return template % (self.vimeo_id)

class Series(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    category = models.ForeignKey(Category)
    is_active = models.BooleanField()

    description = models.TextField(blank=True)
    behind_the_scenes = models.URLField(blank=True, help_text="Vimeo url to behind the scenes footage")
    vimeo_meta = models.ForeignKey(VimeoMeta, blank=True, null=True)

    def __unicode__(self):
        return '%s' % (self.name)

    @app_models.permalink
    def get_absolute_url(self):
        return ('single_series', 'django_artistportfolio.urls', (), {'slug':self.slug})

    @property
    def first_thumbnail(self):
        media = self.media_set.all()[:1]
        if media:
            return media[0].thumbnail.file.url

class CreditName(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return '%s' % (self.name)

class Credit(models.Model):
    series = models.ForeignKey(Series)
    name = models.ForeignKey(CreditName)
    value = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name.name

class Media(models.Model):
    series = models.ForeignKey(Series)
    thumbnail = models.ForeignKey(MediaFile, blank=True, null=True, related_name='thumbnail_set')
    normal = models.ForeignKey(MediaFile, blank=True, null=True, related_name='normal_set')
    zoomed = models.ForeignKey(MediaFile, blank=True, null=True, related_name='zoomed_set')

class Cinema(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    description = models.TextField(blank=True)
    video = models.URLField(blank=True, help_text="Vimeo url")
    vimeo_meta = models.ForeignKey(VimeoMeta, blank=True, null=True)

from listeners import *