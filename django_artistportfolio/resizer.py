import re
from cStringIO import StringIO
# Try to import PIL in either of the two ways it can end up installed.
try:
    from PIL import Image
except ImportError:
    try:
        import Image
    except ImportError:
        # Django seems to silently swallow the ImportError under certain
        # circumstances. Raise a generic exception explaining why we are
        # unable to proceed.
        raise Exception, 'FeinCMS requires PIL to be installed'

from django.utils.encoding import force_unicode
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from feincms.module.medialibrary.models import MediaFile

class Resizer(object):
    THUMBNAIL_SIZE_RE = re.compile(r'^(?P<w>\d+)x(?P<h>\d+)$')
    MARKER = '_resized_'

    def __init__(self, filename, size='200x200'):
        self._filename = filename
        self.size = size

    @property
    def url(self):
        if self.miniature_exists():
            return self.storage.url(self.miniature)

    @property
    def path(self):
        if self.miniature_exists():
            return self.storage.path(self.miniature)

    @property
    def FeinCMS_MediaFile(self):
        if self.miniature_exists():
            return MediaFile.objects.create(file=self.storage.open(self.miniature))

    @property
    def storage(self):
        # figure out storage
        if hasattr(self._filename, 'storage'):
            storage = self._filename.storage
        else:
            storage = default_storage

        return storage

    @property
    def filename(self):
        # figure out name
        if hasattr(self._filename, 'name'):
            filename = self._filename.name
        else:
            filename = force_unicode(self._filename)

        return filename

    @property
    def miniature(self):
        # defining the filename and the miniature filename
        try:
            basename, format = self.filename.rsplit('.', 1)
        except ValueError:
            basename, format = self.filename, 'jpg'

        miniature = u''.join([
            '_resized/',
            basename,
            self.MARKER,
            self.size,
            '.',
            format,
            ])

        return miniature

    def miniature_exists(self):
        match = self.THUMBNAIL_SIZE_RE.match(self.size)
        if not (self.filename and match):
            return u''

        matches = match.groupdict()

        if not self.storage.exists(self.miniature):
            generate = True
        else:
            try:
                generate = self.storage.modified_time(self.miniature) < self.storage.modified_time(self.filename)
            except (NotImplementedError, AttributeError):
                # storage does NOT support modified_time
                generate = False
            except OSError:
                # Someone might have deleted the file
                return u''

        if generate:
            return self.generate(
                storage=self.storage,
                original=self.filename,
                size=matches,
                miniature=self.miniature)

        # Already exists, so we're good
        return True

    def generate(self, storage, original, size, miniature):
        image = Image.open(StringIO(storage.open(original).read()))

        # defining the size
        w, h = int(size['w']), int(size['h'])

        format = image.format # Save format for the save() call later
        image.thumbnail([w, h], Image.ANTIALIAS)
        buf = StringIO()
        if image.mode not in ('RGBA', 'RGB', 'L'):
            image = image.convert('RGBA')
        image.save(buf, format or 'jpeg', quality=80)
        raw_data = buf.getvalue()
        buf.close()

        storage.delete(miniature)
        storage.save(miniature, ContentFile(raw_data))

        return True