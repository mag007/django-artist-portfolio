from django.conf.urls.defaults import patterns, url
from views import IndexView, SeriesView

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view()),
    url(r'^(?P<slug>[\w-]+)/$', SeriesView.as_view(), name='single_series'),
)