from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.conf import settings
from models import Series

class IndexView(TemplateView):
    template_name = 'portfolio/index.html'

    def get_context_data(self, **kwargs):
        return {'all_series': Series.objects.filter(is_active=True)}

    def render_to_response(self, context, **response_kwargs):
        if 'app_config' in getattr(self.request, '_feincms_extra_context', {}):
            return self.get_template_names(), context

        return super(IndexView, self).render_to_response(context, **response_kwargs)


class SeriesView(TemplateView):
    template_name = 'portfolio/series.html'

    def get_context_data(self, **kwargs):
        series = get_object_or_404(Series, slug=kwargs['slug'], is_active=True)
        return {'series':series}

    def render_to_response(self, context, **response_kwargs):
        if 'app_config' in getattr(self.request, '_feincms_extra_context', {}):
            return self.get_template_names(), context

        return super(ConcertView, self).render_to_response(context, **response_kwargs)