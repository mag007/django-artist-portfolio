from distutils.core import setup

setup(
    name='django-artist-portfolio',
    version='0.1.0',
    author='Magnetic Creative',
    author_email='dev@mag.cr',
    packages=['django_artistportfolio'],
    license='LICENSE.txt',
    description='Portfolio app.',
    long_description=open('README.rst').read(),
    install_requires=[
        "Django >= 1.1.1",
    ],
)